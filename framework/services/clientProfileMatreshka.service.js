import supertest from 'supertest';
import { urls } from '../config';
import { decorateService } from '../../lib/decorate';

const ClientProfile = function ClientProfile() {

  this.getProfile = async function (token) {
    reporter.startStep("Вызываем GET /v1/client/profile");

    const r = await supertest(urls.matreshka)
      .get('/v1/client/profile') //метод GET
      .set('Authorization', `Bearer ${token}`)
      .send();

    reporter.endStep();
    return r;
  };


  this.getProfileSummary = async function (token) {
    reporter.startStep("Вызываем GET /v1/client/profile/summary");

    const r = await supertest(urls.matreshka)
      .get('/v1/client/profile/summary') //метод GET
      .set('Authorization', `Bearer ${token}`)
      .send();

    reporter.endStep();
    return r;
  };

};

decorateService(ClientProfile);

export { ClientProfile };