import supertest from 'supertest';
import { urls } from '../config';
import { decorateService } from '../../lib/decorate';

const Authorization = function Authorization() {

  this.post = async function (params) {
    reporter.startStep("Вызываем post /v1/client/login");

    const r = await supertest(urls.matreshka)
      .post('/v1/client/login') //метод POST 
      .send(params);

    reporter.endStep(); 

    return r;
  };

};

decorateService(Authorization);

export { Authorization };