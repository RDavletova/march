import supertest from 'supertest';
import { urls } from '../config';
import { decorateService } from '../../lib/decorate';

const ClientTickets = function ClientTickets() {

  this.buyTicket = async function (token, params) {
    reporter.startStep("Вызываем POST /v1/client/tickets/buy");

    const r = await supertest(urls.matreshka)
      .post('/v1/client/tickets/buy') //метод POST
      .set('Authorization', `Bearer ${token}`)
      .send(params);

    reporter.endStep();
    return r;
  };


  this.SearchTickets = async function (token, params) {
    reporter.startStep("Вызываем POST /v1/client/tickets/search");

    const r = await supertest(urls.matreshka)
      .post('/v1/client/tickets/search') //метод POST
      .set('Authorization', `Bearer ${token}`)
      .send(params);

    reporter.endStep();
    return r;
  };

};

decorateService(ClientTickets);

export {
  ClientTickets
};