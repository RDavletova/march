"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.ClientRefferals = void 0;

var _supertest = _interopRequireDefault(require("supertest"));

var _config = require("../config");

var _decorate = require("../../lib/decorate");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var ClientRefferals = function ClientRefferals() {
  this.GetRefferals = function _callee(token, params) {
    var r;
    return regeneratorRuntime.async(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            reporter.startStep("Вызываем POST /v1/client/referrals");
            _context.next = 3;
            return regeneratorRuntime.awrap((0, _supertest["default"])(_config.urls.matreshka).post('/v1/client/referrals') //метод POST
            .set('Authorization', "Bearer ".concat(token)).send(params));

          case 3:
            r = _context.sent;
            //console.log(r);
            reporter.endStep();
            return _context.abrupt("return", r);

          case 6:
          case "end":
            return _context.stop();
        }
      }
    });
  };

  this.SearchRefferals = function _callee2(token, params) {
    var r;
    return regeneratorRuntime.async(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            reporter.startStep("Вызываем POST /v1/client/referrals/search");
            _context2.next = 3;
            return regeneratorRuntime.awrap((0, _supertest["default"])(_config.urls.matreshka).post('/v1/client/referrals/search') //метод POST
            .set('Authorization', "Bearer ".concat(token)).send(params));

          case 3:
            r = _context2.sent;
            //console.log(r);
            reporter.endStep();
            return _context2.abrupt("return", r);

          case 6:
          case "end":
            return _context2.stop();
        }
      }
    });
  };
};

exports.ClientRefferals = ClientRefferals;
(0, _decorate.decorateService)(ClientRefferals);