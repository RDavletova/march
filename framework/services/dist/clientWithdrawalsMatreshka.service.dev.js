"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.ClientWithdrawls = void 0;

var _supertest = _interopRequireDefault(require("supertest"));

var _config = require("../config");

var _decorate = require("../../lib/decorate");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var ClientWithdrawls = function ClientWithdrawls() {
  this.getUserlimits = function _callee(token) {
    var r;
    return regeneratorRuntime.async(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            reporter.startStep("Вызываем  GET /v1/client/withdrawals/limits");
            _context.next = 3;
            return regeneratorRuntime.awrap((0, _supertest["default"])(_config.urls.matreshka).get('/v1/client/withdrawals/limits') //метод GET
            .set('Authorization', "Bearer ".concat(token)).send());

          case 3:
            r = _context.sent;
            //console.log(r);
            reporter.endStep();
            return _context.abrupt("return", r);

          case 6:
          case "end":
            return _context.stop();
        }
      }
    });
  };

  this.getAllWithdraws = function _callee2(token) {
    var r;
    return regeneratorRuntime.async(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            reporter.startStep("Вызываем GET /v1/client/withdrawals/providers");
            _context2.next = 3;
            return regeneratorRuntime.awrap((0, _supertest["default"])(_config.urls.matreshka).get('/v1/client/withdrawals/providers') //метод GET
            .set('Authorization', "Bearer ".concat(token)).send());

          case 3:
            r = _context2.sent;
            //console.log(r);
            reporter.endStep();
            return _context2.abrupt("return", r);

          case 6:
          case "end":
            return _context2.stop();
        }
      }
    });
  };
};

exports.ClientWithdrawls = ClientWithdrawls;
(0, _decorate.decorateService)(ClientWithdrawls);