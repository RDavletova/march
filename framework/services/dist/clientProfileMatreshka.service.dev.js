"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.ClientProfile = void 0;

var _supertest = _interopRequireDefault(require("supertest"));

var _config = require("../config");

var _decorate = require("../../lib/decorate");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var ClientProfile = function ClientProfile() {
  this.getProfile = function _callee(token) {
    var r;
    return regeneratorRuntime.async(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            reporter.startStep("Вызываем GET /v1/client/profile");
            _context.next = 3;
            return regeneratorRuntime.awrap((0, _supertest["default"])(_config.urls.matreshka).get('/v1/client/profile') //метод GET
            .set('Authorization', "Bearer ".concat(token)).send());

          case 3:
            r = _context.sent;
            //console.log(r);
            reporter.endStep();
            return _context.abrupt("return", r);

          case 6:
          case "end":
            return _context.stop();
        }
      }
    });
  };

  this.updateProfile = function _callee2(token, params) {
    var r;
    return regeneratorRuntime.async(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            reporter.startStep("Вызываем PATCH /v1/client/profile");
            _context2.next = 3;
            return regeneratorRuntime.awrap((0, _supertest["default"])(_config.urls.matreshka).patch('/v1/client/profile') //метод PATCH
            .set('Authorization', "Bearer ".concat(token)).send(params));

          case 3:
            r = _context2.sent;
            //console.log(r);
            reporter.endStep();
            return _context2.abrupt("return", r);

          case 6:
          case "end":
            return _context2.stop();
        }
      }
    });
  };

  this.updateProfileAvatar = function _callee3(token, avatarId) {
    var r;
    return regeneratorRuntime.async(function _callee3$(_context3) {
      while (1) {
        switch (_context3.prev = _context3.next) {
          case 0:
            reporter.startStep("Вызываем  PUT /v1/client/profile/avatar");
            _context3.next = 3;
            return regeneratorRuntime.awrap((0, _supertest["default"])(_config.urls.matreshka).put('/v1/client/profile/avatar') //метод PUT
            .set('Authorization', "Bearer ".concat(token)).send(avatarId));

          case 3:
            r = _context3.sent;
            //console.log(r);
            reporter.endStep();
            return _context3.abrupt("return", r);

          case 6:
          case "end":
            return _context3.stop();
        }
      }
    });
  };

  this.getProfileSummary = function _callee4(token) {
    var r;
    return regeneratorRuntime.async(function _callee4$(_context4) {
      while (1) {
        switch (_context4.prev = _context4.next) {
          case 0:
            reporter.startStep("Вызываем GET /v1/client/profile/summary");
            _context4.next = 3;
            return regeneratorRuntime.awrap((0, _supertest["default"])(_config.urls.matreshka).get('/v1/client/profile/summary') //метод GET
            .set('Authorization', "Bearer ".concat(token)).send());

          case 3:
            r = _context4.sent;
            //console.log(r);
            reporter.endStep();
            return _context4.abrupt("return", r);

          case 6:
          case "end":
            return _context4.stop();
        }
      }
    });
  };
};

exports.ClientProfile = ClientProfile;
(0, _decorate.decorateService)(ClientProfile); //export default new ClientProfile();