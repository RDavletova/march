"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.ClientPromoMaterials = void 0;

var _supertest = _interopRequireDefault(require("supertest"));

var _config = require("../config");

var _decorate = require("../../lib/decorate");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var ClientPromoMaterials = function ClientPromoMaterials() {
  this.pagePromo = function _callee(token, params, type) {
    var r;
    return regeneratorRuntime.async(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            reporter.startStep("Вызываем  POST /v1/promo/page");
            _context.next = 3;
            return regeneratorRuntime.awrap((0, _supertest["default"])(_config.urls.matreshka).post('/v1/promo/page') //метод POST
            .query({
              type: "".concat(type)
            }) // передаем query парметр type
            .set('Authorization', "Bearer ".concat(token)).send(params));

          case 3:
            r = _context.sent;
            //console.log(r);
            reporter.endStep();
            return _context.abrupt("return", r);

          case 6:
          case "end":
            return _context.stop();
        }
      }
    });
  };
};

exports.ClientPromoMaterials = ClientPromoMaterials;
(0, _decorate.decorateService)(ClientPromoMaterials);