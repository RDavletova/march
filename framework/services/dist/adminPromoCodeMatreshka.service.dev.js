"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.AdminPromocode = void 0;

var _supertest = _interopRequireDefault(require("supertest"));

var _config = require("../config");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var AdminPromocode = function AdminPromocode() {
  //
  this.CreatePromoCode = function _callee(token, params) {
    var r;
    return regeneratorRuntime.async(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            reporter.startStep("Вызываем post /v1/admin/promo-codes");
            _context.next = 3;
            return regeneratorRuntime.awrap((0, _supertest["default"])(_config.urls.matreshka).post('/v1/admin/promo-codes') //метод post
            .set('Authorization', "Bearer ".concat(token)).send(params));

          case 3:
            r = _context.sent;
            reporter.endStep();
            return _context.abrupt("return", r);

          case 6:
          case "end":
            return _context.stop();
        }
      }
    });
  };

  this.GetPromoCode = function _callee2(token, promoCodeId) {
    var r;
    return regeneratorRuntime.async(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            reporter.startStep("Вызываем get /v1/admin/promo-codes/{promoCodeId}");
            _context2.next = 3;
            return regeneratorRuntime.awrap((0, _supertest["default"])(_config.urls.matreshka).get('/v1/admin/promo-codes/' + "".concat(promoCodeId)) //метод get  /v1/admin/promo-codes/{promoCodeId}
            .set('Authorization', "Bearer ".concat(token)).send());

          case 3:
            r = _context2.sent;
            reporter.endStep();
            return _context2.abrupt("return", r);

          case 6:
          case "end":
            return _context2.stop();
        }
      }
    });
  };

  this.UpdatePromoCode = function _callee3(token, promoCodeId, params) {
    var r;
    return regeneratorRuntime.async(function _callee3$(_context3) {
      while (1) {
        switch (_context3.prev = _context3.next) {
          case 0:
            reporter.startStep("Вызываем put /v1/admin/promo-codes/{promoCodeId}");
            _context3.next = 3;
            return regeneratorRuntime.awrap((0, _supertest["default"])(_config.urls.matreshka).put('/v1/admin/promo-codes/' + "".concat(promoCodeId)) //метод put  /v1/admin/promo-codes/{promoCodeId}
            .set('Authorization', "Bearer ".concat(token)).send(params));

          case 3:
            r = _context3.sent;
            reporter.endStep();
            return _context3.abrupt("return", r);

          case 6:
          case "end":
            return _context3.stop();
        }
      }
    });
  };
};

exports.AdminPromocode = AdminPromocode;