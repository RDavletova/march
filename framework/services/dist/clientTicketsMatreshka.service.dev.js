"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.ClientTickets = void 0;

var _supertest = _interopRequireDefault(require("supertest"));

var _config = require("../config");

var _decorate = require("../../lib/decorate");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var ClientTickets = function ClientTickets() {
  this.buyTicket = function _callee(token, params) {
    var r;
    return regeneratorRuntime.async(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            reporter.startStep("Вызываем POST /v1/client/tickets/buy");
            _context.next = 3;
            return regeneratorRuntime.awrap((0, _supertest["default"])(_config.urls.matreshka).post('/v1/client/tickets/buy') //метод POST
            .set('Authorization', "Bearer ".concat(token)).send(params));

          case 3:
            r = _context.sent;
            //console.log(r);
            reporter.endStep();
            return _context.abrupt("return", r);

          case 6:
          case "end":
            return _context.stop();
        }
      }
    });
  };

  this.getTicketCount = function _callee2(token) {
    var r;
    return regeneratorRuntime.async(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            reporter.startStep("Вызываем GET /v1/client/tickets/count");
            _context2.next = 3;
            return regeneratorRuntime.awrap((0, _supertest["default"])(_config.urls.matreshka).get('/v1/client/tickets/count') //метод GET
            .set('Authorization', "Bearer ".concat(token)).send());

          case 3:
            r = _context2.sent;
            reporter.endStep();
            return _context2.abrupt("return", r);

          case 6:
          case "end":
            return _context2.stop();
        }
      }
    });
  };

  this.getTicketsMultiplayer = function _callee3(token) {
    var r;
    return regeneratorRuntime.async(function _callee3$(_context3) {
      while (1) {
        switch (_context3.prev = _context3.next) {
          case 0:
            reporter.startStep("Вызываем /v1/client/tickets/multiplier");
            _context3.next = 3;
            return regeneratorRuntime.awrap((0, _supertest["default"])(_config.urls.matreshka).get('/v1/client/tickets/multiplier') //метод GET
            .set('Authorization', "Bearer ".concat(token)).send());

          case 3:
            r = _context3.sent;
            reporter.endStep();
            return _context3.abrupt("return", r);

          case 6:
          case "end":
            return _context3.stop();
        }
      }
    });
  };

  this.SearchTickets = function _callee4(token, params) {
    var r;
    return regeneratorRuntime.async(function _callee4$(_context4) {
      while (1) {
        switch (_context4.prev = _context4.next) {
          case 0:
            reporter.startStep("Вызываем POST /v1/client/tickets/search");
            _context4.next = 3;
            return regeneratorRuntime.awrap((0, _supertest["default"])(_config.urls.matreshka).post('/v1/client/tickets/search') //метод POST
            .set('Authorization', "Bearer ".concat(token)).send(params));

          case 3:
            r = _context4.sent;
            reporter.endStep();
            return _context4.abrupt("return", r);

          case 6:
          case "end":
            return _context4.stop();
        }
      }
    });
  };

  this.buyTrialTicket = function _callee5(token, params) {
    var r;
    return regeneratorRuntime.async(function _callee5$(_context5) {
      while (1) {
        switch (_context5.prev = _context5.next) {
          case 0:
            reporter.startStep("Вызываем POST /v1/client/tickets/trial/buy");
            _context5.next = 3;
            return regeneratorRuntime.awrap((0, _supertest["default"])(_config.urls.matreshka).post('/v1/client/tickets/trial/buy') //метод POST
            .set('Authorization', "Bearer ".concat(token)).send(params));

          case 3:
            r = _context5.sent;
            reporter.endStep();
            return _context5.abrupt("return", r);

          case 6:
          case "end":
            return _context5.stop();
        }
      }
    });
  };

  this.getTicketById = function _callee6(token, ticketId) {
    var r;
    return regeneratorRuntime.async(function _callee6$(_context6) {
      while (1) {
        switch (_context6.prev = _context6.next) {
          case 0:
            reporter.startStep("Вызываем GET /v1/client/tickets/' + `${ticketId}`");
            _context6.next = 3;
            return regeneratorRuntime.awrap((0, _supertest["default"])(_config.urls.matreshka).get('/v1/client/tickets/' + "".concat(ticketId)) //метод GET
            .set('Authorization', "Bearer ".concat(token)).send());

          case 3:
            r = _context6.sent;
            reporter.endStep();
            return _context6.abrupt("return", r);

          case 6:
          case "end":
            return _context6.stop();
        }
      }
    });
  };

  this.SearchTicketWinningHistory = function _callee7(token, ticketId, params) {
    var r;
    return regeneratorRuntime.async(function _callee7$(_context7) {
      while (1) {
        switch (_context7.prev = _context7.next) {
          case 0:
            reporter.startStep("Вызываем POST /v1/client/tickets/' + `${ticketId}` + '/winning-history/page");
            _context7.next = 3;
            return regeneratorRuntime.awrap((0, _supertest["default"])(_config.urls.matreshka).post('/v1/client/tickets/' + "".concat(ticketId) + '/winning-history/page') //метод POST
            .set('Authorization', "Bearer ".concat(token)).send(params));

          case 3:
            r = _context7.sent;
            reporter.endStep();
            return _context7.abrupt("return", r);

          case 6:
          case "end":
            return _context7.stop();
        }
      }
    });
  };
};

exports.ClientTickets = ClientTickets;
(0, _decorate.decorateService)(ClientTickets);