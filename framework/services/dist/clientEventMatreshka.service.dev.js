"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.ClientEvent = void 0;

var _supertest = _interopRequireDefault(require("supertest"));

var _config = require("../config");

var _decorate = require("../../lib/decorate");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var ClientEvent = function ClientEvent() {
  this.getEvent = function _callee(token) {
    var r;
    return regeneratorRuntime.async(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            reporter.startStep("Вызываем GET /v1/client/events");
            _context.next = 3;
            return regeneratorRuntime.awrap((0, _supertest["default"])(_config.urls.matreshka).get('/v1/client/events') //метод GET
            .set('Authorization', "Bearer ".concat(token)).send());

          case 3:
            r = _context.sent;
            //console.log(r);
            reporter.endStep();
            return _context.abrupt("return", r);

          case 6:
          case "end":
            return _context.stop();
        }
      }
    });
  };
};

exports.ClientEvent = ClientEvent;
(0, _decorate.decorateService)(ClientEvent);