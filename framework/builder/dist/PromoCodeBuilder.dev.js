"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.PromoCodeBuilder = void 0;

var _faker = _interopRequireDefault(require("faker"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var PromoCodeBuilder = function PromoCodeBuilder() {
  this.PromoCode_activationDate = function PromoCode_activationDate() {
    this.activationDateTimeInMillis = _faker["default"].date.soon().getTime();
    return this;
  };

  this.PromoCode_code = function PromoCode_code() {
    this.code = _faker["default"].finance.bic();
    return this;
  };

  this.PromoCode_discount_gift = function PromoCode_discount_gift() {
    this.discount = 1;
    return this;
  };

  this.PromoCode_discount = function PromoCode_discount() {
    this.discount = Math.floor(Math.random() * Math.floor(10)) * 10 + 1;
    return this;
  };

  this.PromoCode_discountTypeGift = function PromoCode_discountType() {
    this.discountType = 'GIFT';
    return this;
  };

  this.PromoCode_discountType = function PromoCode_discountType() {
    this.discountType = 'PERCENT_FOR_PRODUCT';
    return this;
  };

  this.PromoCode_disposable = function PromoCode_disposable() {
    this.disposable = _faker["default"].random["boolean"]();
    return this;
  };

  this.PromoCode_expirationDate = function PromoCode_expirationDate() {
    this.expirationDateTimeInMillis = _faker["default"].date.future().getTime();
    return this;
  };

  this.PromoCode_name = function PromoCode_name() {
    this.name = _faker["default"].name.firstName();
    return this;
  };

  this.generate = function generate() {
    var _this = this;

    var fields = Object.getOwnPropertyNames(this);
    console.log('fields:', fields);
    var data = {};
    fields.forEach(function (fieldName) {
      if (_this[fieldName] && typeof _this[fieldName] !== 'function') {
        data[fieldName] = _this[fieldName];
      }
    });
    console.log('data from generate method ', data);
    return data;
  };
};

exports.PromoCodeBuilder = PromoCodeBuilder;