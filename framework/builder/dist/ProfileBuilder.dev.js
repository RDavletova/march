"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.ProfileBuilder = void 0;

var _faker = _interopRequireDefault(require("faker"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var ProfileBuilder = function ProfileBuilder() {
  this.Profile_name = function Profile_name() {
    this.name = _faker["default"].name.firstName();
    return this; // Profile_name.name
  };

  this.Profile_referralCode = function Profile_referralCode() {
    this.referralCode = _faker["default"].finance.bic();
    return this;
  };

  this.Profile_referralUrlVisible = function Profile_referralUrlVisible() {
    this.referralUrlVisible = false;
    return this;
  };

  this.Profile_newPassword = function Profile_newPassword() {
    this.newPassword = 'Password56';
    return this;
  };

  this.Profile_oldPassword = function Profile_oldPassword() {
    this.oldPassword = 'Password56';
    return this;
  };

  this.generate = function generate() {
    var _this = this;

    var fields = Object.getOwnPropertyNames(this);
    console.log('fields:', fields);
    var data = {};
    fields.forEach(function (fieldName) {
      if (_this[fieldName] && typeof _this[fieldName] !== 'function') {
        data[fieldName] = _this[fieldName];
      }
    });
    console.log('data from generate method ', data);
    return data;
  };
};

exports.ProfileBuilder = ProfileBuilder;