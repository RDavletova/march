"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.RefferalsBuilder = void 0;

var _faker = _interopRequireDefault(require("faker"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var RefferalsBuilder = function RefferalsBuilder() {
  function randomInteger(min, max) {
    var rand = min - 0.5 + Math.random() * (max - min + 1);
    return Math.round(rand);
  }

  this.getLimitOfRefferals = function getLimitOfRefferals() {
    this.limit = randomInteger(1, 100);
    return this;
  };

  this.getOffsetOfRefferals = function getOffsetOfRefferals() {
    this.offset = 0;
    return this;
  };

  this.getLinesOfRefferals = function getLinesOfRefferals() {
    this.lines = [1, 2];
    return this;
  };

  this.generate = function generate() {
    var _this = this;

    var fields = Object.getOwnPropertyNames(this);
    console.log('fields:', fields);
    var data = {};
    fields.forEach(function (fieldName) {
      if (_this[fieldName] && typeof _this[fieldName] !== 'function') {
        data[fieldName] = _this[fieldName];
      }
    });
    console.log('data from generate method ', data);
    return data;
  };
};

exports.RefferalsBuilder = RefferalsBuilder;