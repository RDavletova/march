import faker from 'faker';

const ProfileBuilder = function ProfileBuilder() {

  this.Profile_name = function Profile_name() {
    this.name = faker.name.firstName();
    return this; // Profile_name.name
  };

  this.Profile_referralCode = function Profile_referralCode() {
    this.referralCode = faker.finance.bic();
    return this;
  };


  this.Profile_referralUrlVisible = function Profile_referralUrlVisible() {
    this.referralUrlVisible = faker.random.boolean();
    return this;
  };

  
  this.generate = function generate() {
    const fields = Object.getOwnPropertyNames(this);
  
    const data = {};

    fields.forEach((fieldName) => {

      if (this[fieldName] && typeof this[fieldName] !== 'function') {
        data[fieldName] = this[fieldName];
      }
    });
    
    return data;
  }

};

export {
  ProfileBuilder
};