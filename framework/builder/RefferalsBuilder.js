import faker from 'faker';

const RefferalsBuilder = function RefferalsBuilder() {

  function randomInteger(min, max) {
    let rand = min - 0.5 + Math.random() * (max - min + 1);
    return Math.round(rand);
  }

  this.getLimitOfRefferals = function getLimitOfRefferals(flag) {

    if (flag) {
      this.limit = randomInteger(1, 100);
    } else {
      this.limit = 0;
    }

    return this;
  };


  this.getOffsetOfRefferals = function getOffsetOfRefferals() {
    this.offset = 0;
    return this;
  };


  this.getLinesOfRefferals = function getLinesOfRefferals() {
    this.lines = [randomInteger(1, 2)];
    return this;

  };

  
  this.generate = function generate() {
    const fields = Object.getOwnPropertyNames(this);
    console.log('fields:', fields);

    const data = {};

    fields.forEach((fieldName) => {

      if (this[fieldName] && typeof this[fieldName] !== 'function') {
        data[fieldName] = this[fieldName];
      }
    });
    console.log('data from generate method ', data);

    return data;
  }

};

export {
  RefferalsBuilder
};