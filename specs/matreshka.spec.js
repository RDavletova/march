import { apiProvider, USER, USER_FOR_PAYMENT_BY_WALLET } from '../framework';
import { TicketBuilder } from '../framework/builder/TicketBuilder.js';
import { RefferalsBuilder } from '../framework/builder/RefferalsBuilder.js';


test('Authorization client', async () => { 
  const r = await apiProvider().authorization().post(USER);
  expect(r.status).toBe(200);
});


describe('testsuit for clientProfile Controller', () => {

  test('Get profile summary', async () => {//

    const rr = await apiProvider().authorization().post(USER);
    const token = rr.body.result.authToken;

    const r = await apiProvider().clientprofile().getProfileSummary(token);

    expect(r.body.result.id).toBe(99);
    expect(r.body.result.email).toEqual(USER.login);
    expect(r.status).toBe(200);
  });

});


describe('testsuit for clientRefferals Controller', () => { 

  test('Search refferals ', async () => {

    const rr = await apiProvider().authorization().post(USER);
    const token = rr.body.result.authToken;

    const demo = new RefferalsBuilder();
    const params = demo
      .getLimitOfRefferals(true)
      .getLinesOfRefferals()
      .getOffsetOfRefferals()
      .generate();

    const r = await apiProvider().clintrefferals().SearchRefferals(token, params);

    expect(r.status).toBe(200);
  });

});


describe('testsuit for client buy ticket Controller', () => {

  test('buy ticket ', async () => {

    const rr = await apiProvider().authorization().post(USER);
    const token = rr.body.result.authToken;

    const demo = new TicketBuilder();
    const params = demo
      .getAmountTicketForBuy()
      .getPartlyOnlinePaymentSumInKopecks()
      .getPaymentMethod(false)
      .getRedirectUrl()
      .generate();

    const r = await apiProvider().clentickets().buyTicket(token, params);

    expect(r.body.result.status).toEqual('PAYMENT_REQUIRED');
    expect(r.status).toBe(201);
  });



  test('buy ticket by wallet', async () => {
    
    const rr = await apiProvider().authorization().post(USER_FOR_PAYMENT_BY_WALLET);
    const token = rr.body.result.authToken;

    const demo = new TicketBuilder();
    const params = demo
      .getAmountTicketForBuy()
      .getPaymentMethod(true)
      .generate();

    const r = await apiProvider().clentickets().buyTicket(token, params);

    expect(r.body.result.status).toEqual('SUCCESS');
    expect(r.status).toBe(201);
  });



  test('search tickets ', async () => {

    const rr = await apiProvider().authorization().post(USER);
    const token = rr.body.result.authToken;

    const demo = new TicketBuilder();
    const params = demo
      .getLimitOfTickets(true)
      .getOffsetOfTickets()
      .getSourcePayment()
      .getWinningTickets()
      .generate();

    const r = await apiProvider().clentickets().SearchTickets(token, params);

    expect(r.body.result.data[0].source).toEqual(params.source);

    expect(r.status).toBe(400);
  });

});


