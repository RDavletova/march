module.exports = {
  testEnvironment: 'node',
  reporters: [
    'default',  [
      'jest-html-reporters',
      {
        filename: 'report.html',
        expand: true,
        pageTitle: 'Matreshka_Report'
      }
    ]
  ],
  moduleFileExtensions: ['js', 'json'],
  transform: {
    '^.+\\.jsx?$': 'babel-jest',
  },
  testMatch: ['**/specs/*.spec.*'] , // ['**/specs/*.spec.*']
  globals: {
    testTimeout: 50000,
  },
  verbose: true,
  setupFilesAfterEnv: ["jest-allure/dist/setup"]
  
};

// reporters: [
//     'default',  'jest-html-reporters', 
//       ['./node_modules/@testomatio/reporter/lib/adapter/jest.js', { apiKey: '31t36af41bly'}],
    
//   ],




